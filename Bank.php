<?php 
declare(strict_types=1);

use Bank\BankService;

include('Operation.php');
include('src/BankService.php');

class Bank implements BankService
{
    private $saldo;
    private $operations = [];

    public function __construct()
    {
        $this->saldo = 0;
    }

    public function deposit(int $amount): void
    {
        $operation = new Operation($amount, 'deposit', $this->saldo);
        $this->saldo = $this->calculateBilans($operation);
        $this->operations[] = $operation;
    }

    public function withdraw(int $amount): void
    {
        $operation = new Operation($amount, 'withdraw', $this->saldo);
        $this->saldo = $this->calculateBilans($operation);
        $this->operations[] = $operation;
    }

    public function printStatement(): void
    {
        echo "Data\t\t|| Kwota\t|| Saldo\n";
        foreach (\array_reverse($this->operations) as $operation) {  //array_reverse
            // die(\var_dump($operation->date));
            $operation->saldo = $this->calculateBilans($operation);
            echo \sprintf(
                "%s\t|| %d\t\t|| %d\n",
                $operation->date,
                $operation->value,
                $operation->saldo
            );
        }
    }

    private function calculateBilans(Operation $operation): int
    {
        switch ($operation->operation) {
            case 'withdraw':
                $saldo = $operation->saldo - $operation->value;

                break;
            case 'deposit':
                $saldo = $operation->saldo + $operation->value;

                break;
        }

        return $saldo;
    }
}
