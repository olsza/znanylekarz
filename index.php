<?php 
declare(strict_types=1);

// use lib\Bank;

include('Bank.php');

$bank = new Bank();

$bank->deposit(500);
$bank->deposit(1000);
$bank->withdraw(200);
$bank->printStatement();
