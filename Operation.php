<?php 
declare(strict_types=1);

class Operation
{
    public $date;
    public $saldo;
    public $operation;
    public $value;

    public function __construct(int $value = 0, string $operation = 'deposit', int $saldo = 0)
    {
        $this->date = \date('d/m/Y');
        $this->saldo = $saldo;
        $this->value = $value;
        $this->operation = $operation;
    }
}
